package com.practitioner.back01.configuration;

public final class ApiConstants {
	private static final String _CUSTOMER = "/customer";
	private static final String _ACCOUNT = "/account";
	public static final String _CUSTOMER_PATH = "/{customerNumber}";
	private static final String BASE = "/api/v1";
	public static final String API_CUSTOMER = BASE + _CUSTOMER;
	public static final String API_CUSTOMER_ACCOUNT = API_CUSTOMER + _CUSTOMER_PATH + _ACCOUNT;
}
