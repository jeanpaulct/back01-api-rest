package com.practitioner.back01.configuration;

import com.practitioner.back01.repository.AccountRepository;
import com.practitioner.back01.repository.CustomerRepository;
import com.practitioner.back01.repository.memory.AccountRepositoryInMemory;
import com.practitioner.back01.repository.memory.CustomerRepositoryInMemory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Back01Configuration {

	@Bean
	public CustomerRepository customerRepository() {
		return new CustomerRepositoryInMemory();
	}

	@Bean
	public AccountRepository accountRepository() {
		return new AccountRepositoryInMemory();
	}

}
