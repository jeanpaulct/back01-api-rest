package com.practitioner.back01.mapper;

import java.util.List;

import com.practitioner.back01.dto.CustomerDto;
import com.practitioner.back01.repository.model.CustomerModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper {
	CustomerDto mapDto(CustomerModel d);

	CustomerModel mapModel(CustomerDto d);

	List<CustomerDto> mapDto(List<CustomerModel> d);

	List<CustomerModel> mapModel(List<CustomerDto> d);
}
