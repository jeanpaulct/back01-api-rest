package com.practitioner.back01.mapper;

import java.util.List;

import com.practitioner.back01.dto.AccountDto;
import com.practitioner.back01.repository.model.AccountModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AccountMapper {
	AccountModel map(AccountDto d);

	AccountDto map(AccountModel d);

	List<AccountDto> map(List<AccountModel> lst);
}
