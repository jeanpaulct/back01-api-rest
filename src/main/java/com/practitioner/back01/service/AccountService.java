package com.practitioner.back01.service;

import java.util.List;

import com.practitioner.back01.dto.AccountDto;

public interface AccountService {
	List<AccountDto> getAccountsByCustomer(String customer);
}
