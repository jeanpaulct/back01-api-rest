package com.practitioner.back01.service;

import java.util.List;

import com.practitioner.back01.dto.AccountDto;
import com.practitioner.back01.dto.CustomerDto;

public interface CustomerService {
	List<CustomerDto> getAll();

	CustomerDto getById(String customerNumber);

	CustomerDto create(CustomerDto customerDto);

	CustomerDto update(CustomerDto customerDto);

	CustomerDto updatePartial(CustomerDto customerDto);

	void remove(String customerNumber);

	AccountDto addAccountCustomer(String document, AccountDto accountDto);
}
