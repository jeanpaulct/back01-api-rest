package com.practitioner.back01.service.impl;

import java.util.List;

import com.practitioner.back01.dto.AccountDto;
import com.practitioner.back01.dto.CustomerDto;
import com.practitioner.back01.mapper.AccountMapper;
import com.practitioner.back01.mapper.CustomerMapper;
import com.practitioner.back01.repository.AccountRepository;
import com.practitioner.back01.repository.CustomerRepository;
import com.practitioner.back01.repository.model.AccountModel;
import com.practitioner.back01.repository.model.CustomerModel;
import com.practitioner.back01.service.CustomerService;
import com.practitioner.back01.service.exception.CustomerAlreadyExistsException;
import com.practitioner.back01.service.exception.CustomerNotFoundException;

import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

	private final CustomerRepository customerRepository;
	private final CustomerMapper customerMapper;
	private final AccountRepository accountRepository;
	private final AccountMapper accountMapper;

	public CustomerServiceImpl(CustomerRepository customerRepository, CustomerMapper customerMapper, AccountRepository accountRepository, AccountMapper accountMapper) {
		this.customerRepository = customerRepository;
		this.customerMapper = customerMapper;
		this.accountRepository = accountRepository;
		this.accountMapper = accountMapper;
	}

	@Override
	public List<CustomerDto> getAll() {
		return customerMapper.mapDto(customerRepository.getAll());
	}

	@Override
	public CustomerDto getById(String customerNumber) {
		CustomerModel m = customerRepository.findByDocument(customerNumber);
		if (m == null)
			throw new CustomerNotFoundException(customerNumber);
		return customerMapper.mapDto(m);
	}

	@Override
	public CustomerDto create(CustomerDto customerDto) {
		if (customerRepository.findByDocument(customerDto.getCustomerNumber()) != null)
			throw new CustomerAlreadyExistsException(customerDto.getCustomerNumber());
		CustomerModel m = customerMapper.mapModel(customerDto);
		return customerMapper.mapDto(customerRepository.create(m));
	}

	@Override
	public CustomerDto update(CustomerDto customerDto) {
		if (customerRepository.findByDocument(customerDto.getCustomerNumber()) == null)
			throw new CustomerNotFoundException(customerDto.getCustomerNumber());
		CustomerModel m = customerMapper.mapModel(customerDto);
		return customerMapper.mapDto(customerRepository.update(m));
	}

	@Override
	public CustomerDto updatePartial(CustomerDto customerDto) {
		if (customerRepository.findByDocument(customerDto.getCustomerNumber()) == null)
			throw new CustomerNotFoundException(customerDto.getCustomerNumber());
		CustomerModel m = customerMapper.mapModel(customerDto);
		return customerMapper.mapDto(customerRepository.updatePartial(m));
	}

	@Override
	public void remove(String customerNumber) {
		customerRepository.remove(customerNumber);
	}

	@Override
	public AccountDto addAccountCustomer(String document, AccountDto accountDto) {
		CustomerDto customerDto = this.getById(document);
		accountDto.setCustomerDocument(customerDto.getCustomerNumber());
		accountDto.setAvailableBalance(0d);
		AccountModel model = this.accountRepository.createAccount(accountMapper.map(accountDto));
		this.customerRepository.addAccount(customerMapper.mapModel(customerDto), model);
		return accountMapper.map(model);
	}
}
