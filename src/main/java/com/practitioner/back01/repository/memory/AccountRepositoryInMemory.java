package com.practitioner.back01.repository.memory;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.practitioner.back01.repository.AccountRepository;
import com.practitioner.back01.repository.model.AccountModel;

public class AccountRepositoryInMemory extends RepositoryInMemory implements AccountRepository {

	public AccountModel createAccount(AccountModel model) {
		model.setNumberAccount(UUID.randomUUID().toString());
		MEMORY_ACCOUNTS.put(model.getNumberAccount(), model);
		return model;
	}

	@Override
	public List<AccountModel> getByCustomer(String customerDocument) {
		return MEMORY_ACCOUNTS.values()
				.stream()
				.filter(m -> customerDocument.equals(m.getCustomerDocument()))
				.collect(Collectors.toList());
	}
}
