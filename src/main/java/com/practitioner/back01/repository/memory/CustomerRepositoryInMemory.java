package com.practitioner.back01.repository.memory;

import java.util.ArrayList;
import java.util.List;

import com.practitioner.back01.repository.CustomerRepository;
import com.practitioner.back01.repository.model.AccountModel;
import com.practitioner.back01.repository.model.CustomerModel;

public class CustomerRepositoryInMemory extends RepositoryInMemory implements CustomerRepository {

	@Override
	public List<CustomerModel> getAll() {
		return List.copyOf(MEMORY_CUSTOMER.values());
	}


	@Override
	public CustomerModel findByDocument(String document) {
		return MEMORY_CUSTOMER.get(document);
	}

	@Override
	public CustomerModel create(CustomerModel customerModel) {
		MEMORY_CUSTOMER.put(customerModel.getCustomerNumber(), customerModel);
		return customerModel;
	}

	@Override
	public CustomerModel update(CustomerModel customerModel) {
		MEMORY_CUSTOMER.replace(customerModel.getCustomerNumber(), customerModel);
		return customerModel;
	}

	private boolean allowReplace(Object newValue, Object oldValue) {
		return newValue != null && (oldValue == null || !oldValue.equals(newValue));
	}

	@Override
	public CustomerModel updatePartial(CustomerModel customerModel) {
		CustomerModel m = MEMORY_CUSTOMER.get(customerModel.getCustomerNumber());
		if (allowReplace(customerModel.getAge(), m.getAge()))
			m.setAge(customerModel.getAge());

		if (allowReplace(customerModel.getAddress(), m.getAddress()))
			m.setAddress(customerModel.getAddress());

		if (allowReplace(customerModel.getBirthDate(), m.getBirthDate()))
			m.setBirthDate(customerModel.getBirthDate());

		if (allowReplace(customerModel.getEmail(), m.getEmail()))
			m.setEmail(customerModel.getEmail());

		if (allowReplace(customerModel.getFullName(), m.getFullName()))
			m.setFullName(customerModel.getFullName());

		if (allowReplace(customerModel.getPhoneNumber(), m.getPhoneNumber()))
			m.setPhoneNumber(customerModel.getPhoneNumber());

		MEMORY_CUSTOMER.replace(m.getCustomerNumber(), m);
		return m;
	}

	@Override
	public void remove(String document) {
		MEMORY_CUSTOMER.remove(document);
	}

	@Override
	public void addAccount(CustomerModel customer, AccountModel account) {
		if (customer.getAccounts() == null)
			customer.setAccounts(new ArrayList<>());
		customer.getAccounts().add(account.getCustomerDocument());
	}
}
