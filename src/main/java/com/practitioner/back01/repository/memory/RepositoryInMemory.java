package com.practitioner.back01.repository.memory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.practitioner.back01.repository.model.AccountModel;
import com.practitioner.back01.repository.model.CustomerModel;

abstract class RepositoryInMemory {
	static final Map<String, CustomerModel> MEMORY_CUSTOMER = new ConcurrentHashMap<>();
	static final Map<String, AccountModel> MEMORY_ACCOUNTS = new ConcurrentHashMap<>();
}
