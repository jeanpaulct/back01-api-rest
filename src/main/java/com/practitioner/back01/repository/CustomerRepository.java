package com.practitioner.back01.repository;

import java.util.List;

import com.practitioner.back01.repository.model.AccountModel;
import com.practitioner.back01.repository.model.CustomerModel;


public interface CustomerRepository {
	List<CustomerModel> getAll();

	CustomerModel findByDocument(String document);

	CustomerModel create(CustomerModel CustomerModel);

	CustomerModel update(CustomerModel CustomerModel);

	CustomerModel updatePartial(CustomerModel CustomerModel);

	void remove(String document);

	void addAccount(CustomerModel customer, AccountModel account);
}
