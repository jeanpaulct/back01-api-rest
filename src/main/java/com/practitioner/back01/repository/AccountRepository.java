package com.practitioner.back01.repository;

import java.util.List;

import com.practitioner.back01.repository.model.AccountModel;

public interface AccountRepository {
	AccountModel createAccount(AccountModel model);

	List<AccountModel> getByCustomer(String customer);
}
